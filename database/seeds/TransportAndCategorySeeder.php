<?php

use Illuminate\Database\Seeder;
use App\Transport;
use App\Category;

class TransportAndCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transport = [
            ['name' => 'Lopend'],
            ['name' => 'Fiets'],
            ['name' => 'Auto'],
            ['name' => 'Brommer/ scooter'],
            ['name' => 'Anders'],
        ];
        $categories = [
            ['name' => 'Boodschappen', 'icon' => 'shopping-basket'],
            ['name' => 'Dierverzorging', 'icon' => 'dog-leashed'],
            ['name' => 'Ondersteuning thuis', 'icon' => 'house-user'],
            ['name' => 'Klussen', 'icon' => 'house-day'],
            ['name' => 'Anders', 'icon' => 'question'],
        ];

        foreach ($transport AS $transport) {
            Transport::updateOrCreate(['name' => $transport['name']], $transport);
        }

        foreach ($categories AS $category) {
            Category::updateOrCreate(['name' => $category['name']], $category);
        }
    }
}
