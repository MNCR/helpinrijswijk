<?php

use Illuminate\Database\Seeder;
use App\People;

use Faker\Factory as Faker;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('nl_NL');
        foreach (range(1,10) as $index) {
            People::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'email_verified' => true,
                'is_volunteer' => (bool)random_int(0, 1)
            ]);
        }
    }
}
