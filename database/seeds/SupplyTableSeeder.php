<?php

use Illuminate\Database\Seeder;
use App\People;
use App\Supply;
use App\Category;
use App\Transport;
use Faker\Factory as Faker;

class SupplyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('nl_NL');
        $people = People::all();
        $postalCodes = ['1AT', '9CB', '4VR', '2DG', '3AE', '5BC', '6EE', '7SC', '8TJ'];
        $radius = [1,2,4];

        foreach ($people AS $p) {
            $t = Transport::all()->random(1)->first();
            $c = Category::all()->random(1)->first();
            Supply::create([
                'category_id' => $c->id,
                'people_id' => $p->id,
                'transport_id' => $t->id,
                'radius' => $radius[array_rand($radius)],
                'supply_verified' => true,
                'postal_code' => '228' . $postalCodes[array_rand($postalCodes)],
                'is_volunteer' => (bool)random_int(0, 1),
                'description' => $faker->realText(300)
            ]);
        }
    }
}
