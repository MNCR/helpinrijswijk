<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email' => 'michiel@auerbach.nl',
                'name' => 'Michiel Auerbach',
                'password' => bcrypt('MichielsGeheim2020!')
            ],
            [
                'email' => 'helpinrijswijk@gmail.com',
                'name' => 'Help in Rijswijk',
                'password' => bcrypt('HelpElkaarVoorElkaar')
            ],
        ];

        foreach ($users AS $user) {
            User::updateOrCreate(['email' => $user['email']], $user);
        }
    }
}
