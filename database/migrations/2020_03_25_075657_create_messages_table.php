<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('message_id')->nullable();
            $table->unsignedInteger('from_people_id')->nullable();
            $table->unsignedInteger('to_people_id')->nullable();
            $table->unsignedInteger('supply_id')->nullable();
            $table->string('topic')->nullable();
            $table->text('message');
            $table->boolean('is_read')->default(false);
            $table->boolean('is_verified')->default(false);
            $table->string('message_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
