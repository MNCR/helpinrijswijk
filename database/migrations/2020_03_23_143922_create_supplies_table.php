<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('people_id');
            $table->unsignedInteger('transport_id');
            $table->unsignedInteger('category_id');
            $table->string('postal_code');
            $table->string('long')->nullable();
            $table->string('lat')->nullable();
            $table->ipAddress('client_ip')->nullable();
            $table->text('topic')->nullable();
            $table->text('description');
            $table->integer('radius');
            $table->boolean('supply_verified')->default(false);
            $table->string('verify_token')->nullable();
            $table->boolean('is_volunteer')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplies');
    }
}
