<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

App::setLocale('nl');

Auth::routes([ 'register' => false ]);
Route::get('/', 'HomeController@index')->name('home');
Route::get('/verifyemail/{code}', 'HomeController@verifyEmail')->name('verify.email');

//Messages
Route::get('/messages', 'MessageController@messages')->name('messages.index');
Route::get('/messages/check/{code}', 'MessageController@myMessages')->name('messages.my_messages');
Route::get('/messages/sendreply/{message}/{code}', 'MessageController@replyMessage')->name('messages.reply_message');
Route::post('/messages/sendreply/{message}/{code}', 'MessageController@sendReplyMessage')->name('messages.send_reply');
Route::post('/messages/sendemail', 'MessageController@sendEmail')->name('messages.sendEmail');
Route::post('/messages/newmessage', 'MessageController@newMessage')->name('messages.newMessage');

//Supplies
Route::post('/addSupply', 'SupplyController@store')->name('supply.add');
Route::get('/supplies', 'SupplyController@index')->name('supplies.index');
Route::post('/supplies/sendemail', 'SupplyController@sendEmail')->name('messages.sendEmail');
Route::get('/supplies/check/{code}', 'SupplyController@mySupplies')->name('supplies.my_supplies');
Route::get('/supplies/edit/{supply}/{code}', 'SupplyController@edit')->name('supplies.edit');
Route::post('/supplies/edit/{supply}/{code}', 'SupplyController@update')->name('supplies.update');
Route::delete('/supplies/delete/{supply}/{code}', 'SupplyController@destroy')->name('supplies.delete');

//Others
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/hoewerkthet', 'HomeController@howDoesItWork')->name('howdoesitwork');

Route::group(['middleware' => 'auth'], function (){
    Route::get('/home', 'LoggedinController@index')->name('loggedin.home');
});
