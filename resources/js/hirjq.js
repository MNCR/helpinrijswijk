$(function() {
    let theModal = $('.main_modal');
    let theMessageSendButton = $('.btn-send-message-to-supply');

    $('body').on("click", ".send-message", function(e){
        e.preventDefault();
        let supplyID = $(this).data('supply_id');

        //Make sure all the fields are empty
        $('.input_my_name').val('');
        $('.input_my_email').val('');
        $('.input_my_message').val('');
        $('.btn-send-message-to-supply').data('send_supply_id', supplyID);

        theModal.modal({'show' : true });
    });

    $(theMessageSendButton).click(function() {
        let theButton = $(this);
        theButton.attr('disabled', 'disabled');
        let supplyID = $(this).data('send_supply_id');
        let name = $.trim($('.input_my_name').val());
        let email = $.trim($('.input_my_email').val());
        let message = $.trim($('.input_my_message').val());

        if ((!name || (name.length < 3))) {
            alert('Vul je naam in');
            return;
        }
        if ((!email || (email.length < 7))) {
            alert('Vul een (geldig) email adres in');
            return;
        }
        if ((!message || (message.length < 25))) {
            alert('Type een bericht waar je enige uitleg geeft');
            return;
        }

        let csrfToken = $('meta[name="csrf_token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });

        let data = {
            "_token": csrfToken,
            'supply_id': supplyID,
            'name': name,
            'email' : email,
            'message': message,
        };

        $.ajax({
            method: 'POST',
            url: '/messages/newmessage',
            data: data,
        }).done(function( msg ) {
            if (msg.result == 'true' || msg.result) {
                $(this).attr('disabled', false);
                //Make sure all the fields are empty
                $('.input_my_name').val('');
                $('.input_my_email').val('');
                $('.input_my_message').val('');
                $('.btn-send-message-to-supply').data('send_supply_id', 0);
                theModal.modal('hide');
                theButton.attr('disabled', false);
                alert('Je bericht is verstuurd, als jouw e-mail adres nog niet bekend is, dan sturen wij je eerst een verificatie e-mail');
            } else {
                theButton.attr('disabled', false);
                alert('Er ging iets mis');
            }
        });
    })
});
