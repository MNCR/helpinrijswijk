var map;

function initMap(mapDiv) {
    var googleLatAndLong = new google.maps.LatLng(52.0376977, 4.3219738);
    var myStyles =[
        {
            featureType: "poi",
            elementType: "labels",
            stylers: [
                { visibility: "off" }
            ]
        }
    ];
    var mapOptions = {
        zoom : 13,
        center : googleLatAndLong,
        mapTypeId : google.maps.MapTypeId.ROADMAP,
        styles: myStyles,
    };

    map = new google.maps.Map(mapDiv, mapOptions);

    addMarker(googleLatAndLong, {title: 'Rijswijk centrum', icon: "https://maps.google.com/mapfiles/ms/icons/green-dot.png"});
}
function procesMarker(item) {
    var googleLatAndLong = new google.maps.LatLng(item.lat, item.long);
    addMarker(googleLatAndLong, item);
}

function addMarker(latLong, item) {
    var markerOptions = {
        position : latLong,
        map : map,
        title: item.title,
    };
    if (item.icon) {
        markerOptions.icon = { url: item.icon };
    }

    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Rijswijk</h1>'+
        '<div id="bodyContent">'+
        '<p><b>Rijswijk</b>, gemeente in Zuid Holland.</p>'+
        '</div>'+
        '</div>';

    if (item.content) {
        var infowindow = new google.maps.InfoWindow({
            content: item.content
        });
    } else {
        var infowindow = new google.maps.InfoWindow({
            content: item.title
        });
    }

    let marker = new google.maps.Marker(markerOptions);
    markerArray.push(marker);

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
}

var markerArray = [];

var mapDiv = document.getElementById("map");
if (mapDiv) {
    initMap(mapDiv);

    if (addMarkers) {
        addMarkers.forEach(function(item, idx) {
            procesMarker(item);
        });
    }
}
