
let specialAlertButton = document.querySelector('.special-alert-box button.close');
let specialAlert = document.querySelector('.special-alert-box');
if (specialAlertButton) {
    specialAlertButton.addEventListener('click', function() {
        specialAlert.classList.add('hidden');
    })
}

let alertbox = document.querySelector('.supply-form-alert-box');

if (alertbox) {
    let top = document.getElementById('section-signin').offsetTop; //Getting Y of target element
    window.scrollTo(0, top);
}

let supplySearch = document.querySelectorAll('.change-supply-search');
let supplyItems = document.querySelectorAll('.supply-item');

if (supplySearch) {
    supplySearch.forEach(function(item, index) {
        item.addEventListener('click', function() {
            let type = this.dataset.showonly;
            supplyItems.forEach(function(item, index) {
                if (type === 'all') {
                    item.classList.remove('hidden');
                } else if (item.classList.contains(type)) {
                    item.classList.remove('hidden');
                } else {
                    item.classList.add('hidden');
                }
            });
        })
    })
}

let requestButton = document.querySelector('.btn-request');
if (requestButton) {
    requestButton.addEventListener('click', function(e) {
        e.preventDefault();
        this.setAttribute('disabled', '');
        let display = document.querySelector('.special-alert-box');
        let span = display.querySelector('span.message');
        let token = document.querySelector('meta[name="csrf_token"]').getAttribute('content');
        let email = document.querySelector('input[name="email"]').value;
        let whatToCheck = this.dataset.request_what;
        let url = '/messages/sendemail';
        if (!whatToCheck) {
            alert('Something went wrong');
            return;
        } else if (whatToCheck == 'supplies') {
            url = '/supplies/sendemail';
        }

        //TODO :: Maybe make frontend check on email address as wel?
        fetch(url, {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text-plain, */*",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-TOKEN": token
            },
            method: 'post',
            credentials: "same-origin",
            body: JSON.stringify({
                email: email,
            })
        })
        .then((data) => {
            if (data.result === false || data.result === 'false') {
                display.classList.remove('alert-success');
                display.classList.add('alert-danger');
                span.textContent = 'Dat is geen geldig e-mail adres';
            } else {
                display.classList.remove('alert-danger');
                display.classList.add('alert-success');
                span.textContent = 'Indien dat mailadres bij ons bekend is, hebben wij je een e-mail gestuurd. Check eventueel ook je spam-box.';
            }
            display.classList.remove('hidden');
            document.querySelector('input[name="email"]').value = '';
            requestButton.removeAttribute('disabled');
        })
        .catch(function(error) {
            display.classList.remove('alert-success');
            display.classList.add('alert-danger');
            span.textContent = 'Er is iets mis gegaan in het systeem. Probeer het opnieuw';
            document.querySelector('input[name="email"]').value = '';
            requestButton.removeAttribute('disabled');
        });
    })
}

let readMore = document.querySelectorAll('.show-more-messages');
if (readMore) {
    readMore.forEach(function(obj, idx) {
        obj.addEventListener('click', function() {
            let messageID = this.dataset.replies_message_id;
            let moreRow = document.querySelector('[data-reply_id="' + messageID + '"]');
            if (moreRow && moreRow.classList.contains('hidden')) {
                moreRow.classList.remove('hidden');
                obj.querySelector('.readless').classList.remove('hidden');
                obj.querySelector('.readmore').classList.add('hidden');
            } else {
                moreRow.classList.add('hidden');
                obj.querySelector('.readless').classList.add('hidden');
                obj.querySelector('.readmore').classList.remove('hidden');
            }
        });
    });
}

let volunteerSelector = document.querySelector('.wat_doe_je');
if (volunteerSelector) {
    volunteerSelector.addEventListener("change", function() {
        let what = this.value;
        if (what === 'seeker') {
            let objects = document.querySelectorAll('.hide_seeker');
            objects.forEach(function(item, index) {
                item.classList.add('hidden');
            });
        } else {
            let objects = document.querySelectorAll('.hide_seeker');
            objects.forEach(function(item, index) {
                item.classList.remove('hidden');
            });
        }
    })
}

let readMoreButton = document.querySelector('.btn_read_more');
if (readMoreButton) {
    readMoreButton.addEventListener('click', function() {
        let moreDiv = document.querySelector('.read_more_homepage');
        if (moreDiv.classList.contains('hidden')) {
            moreDiv.classList.remove('hidden');
            readMoreButton.innerHTML = 'Lees minder <i class="fal fa-arrow-up"></i>';
        } else {
            moreDiv.classList.add('hidden');
            readMoreButton.innerHTML = 'Lees meer  <i class="fal fa-arrow-down"></i>';
        }
    });
}

let logoutLink = document.querySelector('.logout-link');
if (logoutLink) {
    logoutLink.addEventListener('click', function() {
        document.querySelector('.logout_form').submit();
    });
}

let deleteSupplyButton = document.querySelector('.btn-delete-supply');
if (deleteSupplyButton) {
    deleteSupplyButton.addEventListener('click', function() {
        document.querySelector('.delete_form').submit();
    });
}
