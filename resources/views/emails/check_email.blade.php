@extends('emails.base')

@section('mail_content')
    @php
    $what = 'berichten';
    if ($route == 'supplies.my_supplies')
        $what = 'oproepen';
    @endphp
    Beste {{$people->name}},
    <br />
    <br />
    Onlangs is op onze website verzocht om de {{$what}} gekoppeled met dit e-mail adres te bekijken. Om dit te doen sturen wij altijd
    een verificatie link om er zeker van te zijn dat jij degene bent die bij het e-mail adres hoort.
    <br />
    <br />
    <br />
    <a href="{{route($route, $people->verify_token)}}">Klik hier om je {{$what}} te bekijken</a> <br />
    <br />
    Werkt de link niet, of zie je niets? Kopieer en plak dan onderstaande link in je webbrowser.<br />
    <br />
    {{route($route, $people->verify_token)}}
@endsection
