@extends('emails.base')

@section('mail_content')
    Beste {{$people->name}},
    <br />
    <br />
    Je hebt een bericht gekregen op het Help In Rijswijk platform.
    <br />
    <br />
    <a href="{{route('messages.my_messages', $people->verify_token)}}">Klik hier om het bericht te bekijken</a>.
    Werkt dit niet goed, kopieer en plak dan onderstaande lange URL in je favoriete webbrowser.
    <br />
    {{route('messages.my_messages', $people->verify_token)}}
@endsection
