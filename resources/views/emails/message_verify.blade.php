@extends('emails.base')

@section('mail_content')
    Beste {{$people->name}},
    <br />
    <br />
    Onlangs is op onze website verzocht om de berichten gekoppeled met dit e-mail adres te bekijken. Om dit te doen sturen wij altijd
    een verificatie link om er zeker van te zijn dat jij degene bent die bij het e-mail adres hoort.
    <br />
    <br />
    {{route('messages.my_messages', $people->verify_token)}}
@endsection
