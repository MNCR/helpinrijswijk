@extends('emails.base')

@section('mail_content')
    Beste {{$supply->people->name}},
    <br />
    <br />
    Dank je wel voor je inschrijving. Om er zeker van te zijn dat alle inzendingen goed zijn, vragen we je om (alle) aanmeldingen te bevestigen.
    <br />
    <br />
    Om je aanmelding te bevestigen, klik op onderstaande link:
    <br />
    <br />
    {{route('verify.email', $supply->people->verify_token)}}
    <br />
@endsection


