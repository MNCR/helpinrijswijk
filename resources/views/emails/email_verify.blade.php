@extends('emails.base')

@section('mail_content')
    Beste {{$people->name}},
    <br />
    <br />
        Om je bericht te kunnen versturen willen we graag eerst je e-mail adres verifieren. Door op onderstaande link te klikken verifieer
        jij je email adres en sturen we je bericht door.
    <br />
    <br />
        <a href="{{route('verify.email', $people->verify_token)}}">Klik hier om je email te valideren</a><br />
    <br />
        Kan je niet klikken, kopieer dan onderstaande lange URL in je favoriete webbrowser.<br/>
        {{route('verify.email', $people->verify_token)}}
@endsection
