<!doctype html>
<html lang="nl">
<head>
    <title>Email van Help in Rijswijk</title>
    <style>
        body {
            font-family: Verdana, sans-serif;
            font-size: 12px;
        }
    </style>
</head>
<body>
    @yield('mail_content')
    <br />
    <br />
    Bedankt voor je inzet/ oproep!
    <br />
    <br />
    Met vriendelijke groeten,<br />
    <br />
    Het team van Help in Rijswijk!
    <br />
    <br />
    Dit is een geautomatiseerd bericht via <a href="https://helpinrijswijk.nl">https://helpinrijswijk.nl </a><br />
    Je krijgt dit bericht omdat jij, of iemand anders, jouw e-mail adres gebruikt heeft.
    <br />
    <br />
    Mocht je vragen hebben, kijk op onze website en neem contact met ons op.
    <br />
    <br />
</body>
</html>
