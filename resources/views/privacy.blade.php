@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2>Privacy statement</h2>
            <p>
                helpinrijswijk.nl en hulpinrijswijk.nl zijn gemaakt en bedacht door Wikash Orlando Boedhoe en Michiel Auerbach.
            </p>
            <p>
                Het uitgangspunt is om elkaar te helpen, maar omdat er genoeg mensen zijn die ondanks de situatie iets voor een ander willen betekenen hebben wij het voor u mogelijk gemaakt.
            </p>
            <h3>Wat bewaren we</h3>
            <p>
            <ul>
                <li>Je naam</li>
                <li>Je e-mail adres</li>
                <li>Je postcode (indien je die opgegeven hebt, maar voor de basis van de tool is dit verplicht</li>
                <li>Je IP adres (dit in verband met hack/ nep accounts te blokkeren</li>
                <li>Berichten die je post</li>
                <li>Alle verdere gegevens die in het formulier staan</li>
            </ul>
            </p>
            <h3>Wat doen we ermee?</h3>
            <p>
                Hier kunnen we heel kort over zijn, helemaal niets.<br />
                Je data wordt niet verkocht, de website houdt geen cookies bij (ook geen Google statistieken/ analytische gegevens).<br />
                Wel gebruiken we oplossingen van Google om bijvoorbeeld je GPS-coordinaten op te halen vanuit de postcode en wordt er
                gebruik gemaakt van Google Maps op de begin pagina om alle punten te laten zien.
            </p>
            <h3>Is de data veilig?</h3>
            <p>
                We doen ons best om de website veilig te houden. Een is dat de verbinding veilig is (dit is te zien aan het slotje voor
                het webadres in je webbrowser.
            </p>
            <p>We houden ook de server waar alles op draait in de gaten en doen ons best om ook daar hoge veiligheidseisen in het gereel te houden</p>
            <p>
                We hebben een drietal rapporten opgeslagen als PDF om jullie de veiligheid aan te tonen;<br />
                <ul>
                    <li><a href="/images/guardian_360_quickscan.pdf" target="_blank">Guardian 360 raport</a></li>
                    <li><a href="/images/qualys_ssl_report.pdf" target="_blank">Qualys SSLLabs Report</a></li>
                    <li><a href="/images/ssltrust_report.pdf" target="_blank">SSL Trust report</a></li>
                </ul>
            </p>
            <h3>Ik heb een suggestie of een klacht</h3>
            <p>
                We horen graag van je, om je goed van dienst te zijn kan je ons 24x7 email op:
            </p>
            <p><a href="mailto:info@helpinrijswijk.nl">info @ helpinrijswijk . nl</a> (klik om te mailen)</p>
            <h3>Wordt er nog aan de site gewerkt?</h3>
            <p>
                Ja, de site is continue in beweging. We wilden met de laatste maatregelen de site snel en veilig online
                hebben. We werken nog steeds in de avonduren hard door om de site nog mooier en beter te maken.<br />
                Heb je ideeeen, suggesties of verbeteringen? Mail ze naar ons, het mail adres vind je onderaan deze pagina.
            </p>
        </div>
    </div>
</div>
@endsection
