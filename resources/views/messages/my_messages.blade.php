@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-lg-8 section-heading">
            <h2><i class="fal fa-comments-alt"></i> Jouw berichten</h2>
            <p>Welkom (terug) {{$people->name}}.</p>
            <p>Hier vind je een overzicht van berichten die naar jouw gestuurd zijn, of die van jou zijn.</p>
            @foreach($messages AS $message)
                <div class="row message__container" data-message_id="{{$message->id}}">
                    <div class="col-4"><span class="text-dark">Onderwerp: </span></div>
                    <div class="col-8">
                        <h6 class="text-info">
                            @if (!empty($message->topic)) {{trim($message->topic)}} @else {{trim($message->supply->category->name)}} @endif
                        </h6>
                    </div>
                    <div class="col-4"><span class="text-dark">Verstuurd op: </span></div>
                    <div class="col-8">
                        <h6 class="text-info">
                            {{$message->created_at->format('d-m-Y')}}
                        </h6>
                    </div>
                    <div class="row mx-1">
                        <div class="col-5 text-info">
                            Van
                            @if ($people->id === $message->from_people_id)
                                {{$message->toPeople->name}}
                            @else
                                {{$message->fromPeople->name}}
                            @endif
                        </div>
                        <div class="col-5 text-success text-right">
                            Van jou
                        </div>
                        <div class="col-12">
                            <div class="alert col-7 @if ($people->id === $message->from_people_id) alert-success float-right @else alert-info @endif">
                                {{$message->message}}
                                <br /><br/>
                                <small>{{$message->created_at->format('d/m/Y H:i')}}</small>
                            </div>
                        </div>
                        @if (count($message->messages) > 0)
                        <div class="row hidden" data-reply_id="{{$message->id}}">
                        @foreach($message->messages AS $reply)
                            <div class="col-12 ">
                                <div class="alert col-7 @if ($people->id === $reply->from_people_id) alert-success float-right @else alert-info @endif">
                                    {{$reply->message}}
                                    <br /><br/>
                                    <small>{{$reply->created_at->format('d/m/Y H:i')}}</small>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        <div class="col-12 text-danger pointer show-more-messages" data-replies_message_id="{{$message->id}}">
                            <span class="readmore"><i class="fal fa-eye"></i> Klik om alles te lezen</span>
                            <span class="readless hidden"><i class="fal fa-eye-slash"></i> Klik om het in te vouwen</span>
                        </div>
                        @else
                            <div class="col-12 text-success"><i class="fal fa-thumbs-up"></i> Er zijn geen reacties</div>
                        @endif
                        <div class="col-12 text-primary">
                            <a href="{{route('messages.reply_message', [$message, $code])}}">
                                <i class="fal fa-envelope"></i> Stuur een reactie
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
