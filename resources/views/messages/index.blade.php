@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2><i class="fal fa-comments-alt"></i> Berichten</h2>
            <p>
                Om toegang te krijgen tot je berichten met andere mensen sturen we je altijd eerst een verificatie e-mail.
            </p>
            <p>
                Dit om de veiligheid te waarborgen en te zorgen dat niet zomaar iemand zaken doet.<br />
            </p>
            <p>
                <div class="form-group col-md-12">
                    <label for="email" class="label">Voer jouw email adres in</label>
                    <div class="form-field-icon-wrap">
                        <span class="icon ion-email"></span>
                        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-primary btn-outline-primary btn-block btn-request" data-request_what="messages" value="Bekijk jouw berichten">
                    </div>
                </div>
            </p>
        </div>
    </div>
</div>

@endsection
