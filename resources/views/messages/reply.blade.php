@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2><i class="fal fa-comment-dots"></i> Reageer op een bericht</h2>
            <p>Hier kan je reageren op een bericht</p>
            <div class="row">
                <div class="col-md-4">
                    <h6 class="text-info">
                        @if (!empty($message->topic))
                            {{$message->topic}}
                        @else
                            {{$message->supply->category->name}}
                        @endif
                    </h6>
                </div>
                <div class="col-md-5">
                    <h6 class="text-info">
                        {{$message->fromPeople->name}}
                    </h6>
                </div>
                <div class="col-md-3">
                    <h6 class="text-info">
                        {{$message->created_at->format('d-m-Y')}}
                    </h6>
                </div>
                <div class="col-md-1"></div>
                <div class="col-11">
                    {{$message->message}}
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-11">
                    <div class="row more-messages" data-reply_id="{{$message->id}}">
                        @foreach($message->messages AS $reply)
                            <div class="col-md-3">{{$reply->created_at->format('d-m-Y')}}</div>
                            <div class="col-md-9">{{$reply->message}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger supply-form-alert-box">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4>Reageer op dit bericht/ deze berichten</h4>
            <form method="POST" action="{{route('messages.send_reply', [$message, $code])}}">
                {{ @csrf_field() }}
                <div class="form-group col-md-12">
                    <label for="bericht" class="label">Type je bericht</label>
                    <div class="form-field-icon-wrap">
                        <textarea rows="10" class="form-control" name="bericht" placeholder="Schrijf een korte reactie">@if (!empty(old('omschrijving'))) {{old('omschrijving') }}  @endif</textarea>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-primary btn-outline-primary btn-block" value="Verstuur dit bericht">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
