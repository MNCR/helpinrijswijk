<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>Help elkaar in Rijswijk</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


</head>
<body data-spy="scroll" data-target="#ftco-navbar-spy" data-offset="0" @if(isset($homepage)) class="homepage" @endif>

<div class="site-wrap">
    @include('layouts.menu')

    <header class="site-header @if(!isset($homepage)) scrolled awake @endif">
        <div class="alert alert-success fade show hidden special-alert-box">
            <span class="message">
                1234
            </span>
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @if (session('supply_stored'))
            <div class="alert alert-success alert-dismissible fade show">
                Je aanmelding is opgeslagen en nu zichtbaar voor iedereen. Dank je wel voor je bijdrage!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('supply_added'))
            <div class="alert alert-warning alert-dismissible fade show">
                Ontzettend bedankt voor je aanmelding! Voor alle opdrachten sturen wij altijd een
                verificatie email. We verzoeken je om je e-mail box (eventueel spam) in de gaten te houden
                en op de verificatie link te klikken.<br />
                Namens alle vrijwilligers en mensen die hulp nodig hebben, je bent een topper!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('email_verified'))
            <div class="alert alert-warning alert-dismissible fade show">
                Je email adres is geverifieerd. Eventuele berichten die je hebt gestuurd zijn
                nu door gestuurd naar de juiste mensen.<br />
                Dank je wel!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('error_message'))
            <div class="alert alert-danger alert-dismissible fade show">
                {{session('error_message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('success_message'))
            <div class="alert alert-success alert-dismissible fade show">
                {{session('success_message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row align-items-center">
            <div class="col-1 col-md-3">
            </div>
            <div class="col-5 col-md-6 text-left site-logo-wrap">
                <a href="{{route('home')}}" class="btn btn-outline-light btn-sm @if(isset($homepage)) homepage_button @endif">Ga naar het begin</a>
            </div>
            <div class="col-5 col-md-3 text-right menu-burger-wrap">
                <a href="#" class="site-nav-toggle js-site-nav-toggle"><i></i></a>
            </div>
        </div>
    </header> <!-- site-header -->
    <div class="main-wrap " id="section-home">
        @if(!isset($homepage))
            <div class="section pb-3 bg-white" id="section-messages" data-aos="fade-up">
        @endif
            @yield('content')
        @if(!isset($homepage))
            </div>
        @endif
    </div>
    <footer class="ftco-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-5">
                    <div class="footer-widget">
                        <h3 class="mb-4">Over ons</h3>
                        <p>
                            Help/ hulp in Rijswijk is een initiatief van een aantal mensen in en rond Rijswijk die het belangrijk vinden
                            om, zeker in deze tijd maar ook daarna, te zorgen dat mensen elkaar vinden en kunnen helpen.
                        </p>
                        <h3 class="mb-4">Privacy</h3>
                        <p>
                            We gaan voorzicht om met je data, <a href="{{route('privacy')}}">Lees meer...</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="footer-widget">
                        <h3 class="mb-4">Vrijwilligers</h3>
                        <p>
                            Wil jij zelf of met je bedrijf helpen? Meld je aan.
                        </p>
                        <h3 class="mb-4">
                            Hulp nodig?
                        </h3>
                        <p>
                            Geef je op wat je nodig hebt!
                        </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="footer-widget">
                        <h3 class="mb-4">Volg ons op Facebook</h3>
                        <ul class="list-unstyled social">
                            <li><a href="https://www.facebook.com/groups/coronahulpteam/learning_content/" target="_blank"><span class="fab fa-facebook"></span></a></li>
                        </ul>
                        <h3 class="mb-4">
                            Contact?
                        </h3>
                        <p>
                            Mail ons op <a href="mailto:info@helpinrijswijk.nl">info @ helpinrijswijk . nl</a>
                        </p>
                        <p>
                            Of bel met Wikash <a href="tel:+31616065926">(06) 16 06 59 26</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row pt-5">
                <div class="col-md-12 text-center">
                    <p>
                        Copyright &copy; 2020+
                        All rights reserved | This site is made with
                        <i class="fal fa-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                        webapp build by Michiel Auerbach
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="modal main_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-danger">Verstuur een bericht</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Leuk dat je wilt helpen en een bericht wilt sturen. Type een korte text, je naam en e-mail adres.</p>
                <div class="row">
                    <div class="form-group col-md-5">
                        <label for="naam" class="label">Je naam</label>
                        <div class="form-field-icon-wrap">
                            <span class="icon ion-android-person"></span>
                            <input type="text" class="form-control input_my_name" name="my_name">
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="email" class="label">Je mailadres</label>
                        <div class="form-field-icon-wrap">
                            <span class="icon ion-email"></span>
                            <input type="email" class="form-control input_my_email" name="my_email">
                        </div>
                    </div>
                    <div class="form-group col-md-10">
                        <label for="beschrijving" class="label">Bericht</label>
                        <div class="form-field-icon-wrap">
                            <textarea class="form-control input_my_message" name="beschrijving" placeholder="Schrijf een kort bericht"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn-send-message-to-supply" data-send_supply_id="0">Verstuur dit bericht</button>
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Sluit de popup</button>
            </div>
        </div>
    </div>
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#ff7a5c"/></svg></div>
</div>

    <script src="/js/site/jquery-3.2.1.min.js"></script>
    <script src="/js/site/jquery-migrate-3.0.1.min.js"></script>
    <script src="/js/site/popper.min.js"></script>
    <script src="/js/site/bootstrap.min.js"></script>
    <script src="/js/site/owl.carousel.min.js"></script>
    <script src="/js/site/jquery.waypoints.min.js"></script>

    <script src="/js/site/bootstrap-datepicker.js"></script>
    <script src="/js/site/jquery.timepicker.min.js"></script>
    <script src="/js/site/jquery.stellar.min.js"></script>

    <script src="/js/site/jquery.easing.1.3.js"></script>

    <script src="/js/site/aos.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_GEOCODING_API_KEY')}}" ></script>
    <script src="/js/site/main.js"></script>

</body>
</html>
