<nav class="site-menu" id="ftco-navbar-spy">
    <div class="site-menu-inner" id="ftco-navbar">
        <ul class="list-unstyled">
            <li><i class="fal fa-home fa-2x"></i> <a href="{{route('home')}}#section-home"> Home</a></li>
            <li><i class="fal fa-wave-sine fa-2x"></i> <a href="{{route('home')}}#section-welkom"> Welkom</a></li>
            <li><i class="fal fa-sign-in-alt fa-2x"></i> <a href="{{route('home')}}#section-meldjeaan"> Meld je aan</a></li>
            <li><i class="fal fa-list-alt fa-2x"></i> <a href="{{route('home')}}#section-overzicht"> Overzicht</a></li>
            <li><i class="fal fa-map-signs fa-2x"></i> <a href="{{route('home')}}#map"> Kaart weergave</a></li>
            <li><i class="fal fa-comments-alt fa-2x"></i> <a href="{{route('messages.index')}}"> Mijn berichten</a></li>
            <li><i class="fal fa-calendar-edit fa-2x"></i> <a href="{{route('supplies.index')}}"> Mijn oproepen</a></li>
            <li><i class="fal fa-map-marker-question fa-2x"></i> <a href="{{route('howdoesitwork')}}"> Hoe werkt het?</a></li>
            <li><i class="fal fa-user-secret fa-2x"></i> <a href="{{route('privacy')}}"> Privacy</a></li>
            @if (!Auth::guest())
                <li><i class="fal fa-portal-exit fa-2x"></i> <a href="#" class="logout-link"> Logout</a></li>
                <form class="logout_form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </ul>
    </div>
</nav>
