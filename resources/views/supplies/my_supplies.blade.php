@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2>Jouw oproepen</h2>
            <p>Hier vindt je een overzicht van al jouw oproepen.</p>
            @foreach($supplies AS $supply)
                <div class="row message__container mb-3" data-message_id="{{$supply->id}}">
                    <div class="col-md-4">
                        <h6>Categorie:</h6>
                        <h6 class="text-info">{{$supply->category->name}}</h6>
                    </div>
                    <div class="col-md-4">
                        <h6>Onderwerp</h6>
                        <h6 class="text-info">{{$supply->topic}}</h6>
                    </div>
                    <div class="col-md-3">
                        <h6>Radius</h6>
                        <h6 class="text-info">
                            @if ($supply->radius === 1)
                                < 5km
                            @elseif ($supply->radius === 2)
                                5 - 10km
                            @else
                                10 - 20km
                            @endif
                        </h6>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-11">
                        {{$supply->description}}
                        <br />
                        <small>Geplaatst op: {{$supply->created_at->format('d-m-Y')}} om {{$supply->created_at->format('H:i')}}</small>
                    </div>
                    <div class="col-12 py-3">
                            <a href="{{route('supplies.edit', [$supply, $code])}}" class="btn btn-outline-info"><i class="fal fa-pencil-alt"></i> Bewerk deze oproep</a>
                            <button class="btn btn-outline-danger btn-delete-supply" data-usercode="{{$code}}" data-supply_id="{{$supply->id}}"> <i class="fal fa-trash"></i> Wis deze oproep</button>
                            <form method="POST" action="{{route('supplies.delete', [$supply, $code])}}" class="delete_form">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
