@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2><i class="fal fa-calendar-edit"></i> Oproepen</h2>
            <p>
                Om een overzicht te krijgen van al jouw geplaatste oproepen vragen we je eerst ter verificatie je e-mail adres
                in te vullen.<br />
                Na het invullen van je e-mail sturen wij je een verificatie e-mail met een link. Deze link stuurt je terug naar
                de homepage en toont al jouw oproepen.
            </p>
            <p>
                Deze extra veiligheid zorgt ervoor dat niet iemand zomaar met jouw oproepen aan de haal gaat.
            </p>
            <p>
                <div class="form-group col-md-12">
                    <label for="email" class="label">Voer jouw email adres in</label>
                    <div class="form-field-icon-wrap">
                        <span class="icon ion-email"></span>
                        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-primary btn-outline-primary btn-block btn-request" data-request_what="supplies" value="Bekijk jouw oproepen">
                    </div>
                </div>
            </p>
        </div>
    </div>
</div>
@endsection
