@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2><i class="fal fa-pencil-alt"></i> Bewerk je oproep</h2>
            <p>
                Hier kan je de door jou gekozen oproep bewerken.
                @if ($errors->any())
                    <div class="alert alert-danger supply-form-alert-box">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </p>
            <p>
                <form method="POST" action="{{route('supplies.update', [$supply, $code])}}">
                    {{csrf_field()}}
                    <div class="row mb-4">
                        <div class="form-group col-md-6">
                            <label for="wat_doe_je" class="label">Biedt je hulp aan of zoek je het?</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                @php
                                    $what = 'seeker';
                                    if ($supply->is_volunteer)
                                        $what = 'volunteer';
                                @endphp
                                <select disabled readonly name="wat_doe_je" id="wat_doe_je" class="form-control wat_doe_je">
                                    <option @if(old('wat_doe_je', $what) == "volunteer") selected @endif value="volunteer">&nbsp;Ik bied mijn hulp aan</option>
                                    <option @if(old('wat_doe_je', $what) == "seeker") selected @endif value="seeker">&nbsp;Ik heb hulp nodig</option>
                                </select>
                                <small>Dit veld kan hier niet aangepast worden i.v.m. mogelijke berichten.</small>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="radius" class="label hide_seeker">Vanuit welke straal?</label>
                            <div class="form-field-icon-wrap hide_seeker">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="radius" id="radius" class="form-control">
                                    <option @if(old('radius', $supply->radius) == 1) selected @endif value="1">&nbsp;< 5 km</option>
                                    <option @if(old('radius', $supply->radius) == 2) selected @endif value="2">&nbsp;5-10km</option>
                                    <option @if(old('radius', $supply->radius) == 4) selected @endif value="4">&nbsp;10-20km</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="categorie" class="label">Wat zoek je/ heb je nodig?</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="categorie" id="categorie" class="form-control">
                                    @foreach ($categories AS $category)
                                        <option @if(old('categorie', $supply->category_id) == $category->id) selected @endif value="{{$category->id}}">&nbsp {{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="vervoer" class="label hide_seeker">Hoe kan je het doen?</label>
                            <div class="form-field-icon-wrap hide_seeker">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="vervoer" id="vervoer" class="form-control">
                                    @foreach ($transports AS $transport)
                                        <option @if(old('vervoer', $supply->transport_id) == $transport->id) selected @endif value="{{$transport->id}}">&nbsp {{$transport->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="onderwerp" class="label">Onderwerp</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-onderwerp"></span>
                                <input type="text" class="form-control" id="onderwerp" name="onderwerp" value="{{old('onderwerp', $supply->topic)}}" placeholder="Schrijf een kort onderwerp">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="beschrijving" class="label">Beschrijving</label>
                            <div class="form-field-icon-wrap">
                                <textarea class="form-control" name="beschrijving" placeholder="Beschrijf kort wat je aanbiedt of zoekt">@if (!empty(old('omschrijving', $supply->description))) {{old('omschrijving', $supply->description) }}  @endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <input type="submit" class="btn btn-primary btn-outline-primary btn-block" value="Bewerk deze oproep">
                        </div>
                    </div>
                </form>
            </p>
        </div>
    </div>
</div>
@endsection
