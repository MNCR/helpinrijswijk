@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-center">
        <div class="col-md-12 col-lg-8 section-heading">
            <h2>Hoe werkt het?</h2>
            <div class="row">
                <div class="col-4 col-md-2">
                    <i class="fal fa-clipboard-list fa-5x"></i>
                </div>
                <div class="col-8 col-md-10">
                    <h5 class="text-danger">Registreer of je wilt helpen, of hulp nodig hebt.</h5>
                    Iedereen kan zich inschrijven, heb je hulp nodig, of biedt je hulp aan.
                    Geef op wat en waar je het zoekt.
                </div>

                <div class="col-8 col-md-10 pt-4">
                    <h5 class="text-danger">We controleren je email adres</h5>
                    Je krijgt na je inschrijving een email toegestuurd om je email adres te controleren.
                    Dit doen we bij elke aanbieding opnieuw. Zo weten we zeker dat jij bent wie je zegt
                    dat je bent, en dat emails goed aankomen.
                </div>
                <div class="col-4 col-md-2 pt-4">
                    <i class="fal fa-user-check fa-5x"></i>
                </div>


                <div class="col-4 col-md-2  pt-4">
                    <i class="fal fa-map-marker-check fa-5x"></i>
                </div>
                <div class="col-8 col-md-10  pt-4">
                    <h5 class="text-danger">Je oproep staat online</h5>
                    Je oproep komt in de lijst op onze website, en mensen kunnen reageren.
                    Als je postcode bekend is bij Google, plaatsen we ook een marker op de Google Maps onderaan de website.
                </div>

                <div class="col-8 col-md-10  pt-4">
                    <h5 class="text-danger">Mensen kunnen reageren</h5>
                    Mensen die aan jouw oproep gehoor geven kunnen reageren. Door op de link te klikken krijgen zij een popup op
                    de website en kunnen zij reageren.
                </div>
                <div class="col-4 col-md-2  pt-4">
                    <i class="fal fa-comment-smile fa-5x"></i>
                </div>


                <div class="col-4 col-md-2  pt-4">
                    <i class="fal fa-user-check fa-5x"></i>
                </div>
                <div class="col-8 col-md-10  pt-4">
                    <h5 class="text-danger">Ook dit e-mail adres controleren wij</h5>
                    Als je hebt gereageerd controleren wij ook dat email adres. Is dit email adres al bekend en gecontroleerd
                    slaan we deze stap, bij reageren op berichten over. Dit omdat berichten alleen te lezen zijn via een
                    link in de e-mail.
                </div>

                <div class="col-8 col-md-10  pt-4">
                    <h5 class="text-danger">Praat met elkaar</h5>
                    Via de website kan je vervolgens over en weer berichten sturen. Let op, wees voorzichtig met het snel
                    delen van persoonlijke informatie. Zorg dat je mensen eerst spreekt. <br />
                    <br />
                    <span class="text-primary">Geef nooit zomaar je pinpas, pincode of cashgeld mee!</span>
                </div>
                <div class="col-4 col-md-2  pt-4">
                    <i class="fal fa-comments fa-5x"></i>
                </div>

                <div class="col-4 col-md-2  pt-4">
                    <i class="fal fa-handshake fa-5x"></i>
                </div>
                <div class="col-8 col-md-10  pt-4">
                    <h5 class="text-danger">Kom tot overeenstemming</h5>
                    Ben je het eens, spreek dan af en help elkaar!<br />
                    <br />
                    Maak goede afspraken, en houdt je aan de veiligheidsregels.
                </div>

                <div class="col-8 col-md-10  pt-4">
                    <h5 class="text-danger">Klaar!</h5>
                    Zo simpel is het.
                </div>
                <div class="col-4 col-md-2  pt-4">
                    <i class="fal fa-flag-checkered fa-5x"></i>
                </div>

                <div class="col-12  pt-4">
                    Dus waar wacht je op, <i class="fal fa-sign-in-alt"></i> <a href="{{route('home')}}#section-meldjeaan"> Meld je aan</a> (klik om je aan te melden)
                </div>
            </div>
    </div>
</div>
@endsection
