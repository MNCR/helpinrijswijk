@extends('layouts.app')

@section('content')

<div class="cover_1 overlay bg-light">
    <div class="img_bg" style="background-image: url(images/header_two.jpg);" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-10" data-aos="fade-up">
                    <h2 class="heading mb-5">Help in Rijswijk</h2>
                    <p>
                        <a href="#section-meldjeaan" class="smoothscroll btn btn-outline-white px-5 py-3">
                            Meld je aan!
                        </a>
                    </p>
                    <p><h4><a href="#section-welkom" class="smoothscroll">Klik hier voor meer informatie</a></h4></p>
                </div>
            </div>
        </div>
    </div>
</div> <!-- .cover_1 -->

<div class="section pb-3 bg-white" id="section-welkom" data-aos="fade-up">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-12 col-lg-8 section-heading">
                <h2>Welkom</h2>
                <div class="row">
                    <div class="col-3 col-md-2">
                        <img src="/images/logo.png" class="header-logo">
                    </div>
                    <div class="col-md-10 col-8">
                    <p>
                        Welkom op de Help in Rijswijk website. Super leuk dat u zich sterk wilt maken om een ander te helpen! Deze website is gemaakt omdat wij mensen zoals u nodig hebben om in de samenleving een helpende hand te bieden tijdens het Corona virus. Maar ook in de toekomst.
                    </p>
                    <p>
                        <a href="{{route('howdoesitwork')}}" target="_blank" class="btn btn-link">
                            Klik hier voor een uitleg
                        </a>
                    </p>
                    <div class="read_more_container text-right">
                        <button class="btn btn-outline-primary btn-sm btn_read_more">Lees meer <i class="fal fa-arrow-down"></i></button>
                    </div>
                </div>
                <div class="read_more_homepage hidden">
                    <div class="col-12 col-md-8">
                        <h3>Het Doel</h3>
                        <p>
                            Iedereen heeft weleens hulp nodig. Zeker rond om het Corona virus merken wij dat er dringend behoefte is aan hulp. Om andere niet te vergeten is ons doel om hen te helpen bij dagelijkse benodigdheden. Iedereen kan hier aan mee doen. Alle kleine beetjes helpen mee. Dit kan voor zowel u als anderen waardevol zijn!
                        </p>
                        <h3>Wie kunnen helpen?</h3>
                        <p>
                            Iedereen die iets voor een ander wil en kan doen in deze rare tijd kan helpen. Thuiswerkers, studenten/ scholieren, jongeren maar ook bedrijven en instellingen die kunnen helpen of hulp nodig hebben kunnen zich opgeven.<br />
                            Denk hierbij aan:
                            <ul>
                                <li>boodschappen doen</li>
                                <li>gratis maaltijden bezorgen</li>
                                <li>huisdier verzorgen of uitlaten</li>
                                <li>medicatie ophalen</li>
                                <li>ondersteuning of begeleiding</li>
                            </ul>
                            Zo kunnen er meerdere vormen van hulp aan te pas komen.
                        </p>
                        <h3>Voor welk doelgroep?</h3>
                        <p>
                            Iedereen die hulp nodig heeft met bijvoorbeeld fysieke of medische klachten, kunnen hier hun oproep doen. Dit geldt ook voor overige doelgroepen die hulp willen bieden aan een ander. Vergeet dit niet te doen, want er zijn genoeg mensen die op uw hulp wachten!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- .section -->

<div class="section bg-light" data-aos="fade-up" id="section-meldjeaan">
    <div class="container">
        <div class="row section-heading justify-content-center mb-5">
            <div class="col-md-8 text-center">
                <h2 class="heading mb-3">Meld je aan</h2>
                <p class="sub-heading mb-5">Hulp nodig, of biedt je hulp, geef je op!</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10 p-5 form-wrap">
                @if ($errors->any())
                    <div class="alert alert-danger supply-form-alert-box">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{route('supply.add')}}">
                    {{ @csrf_field() }}
                    <div class="row mb-4">
                        <div class="form-group col-md-6">
                            <label for="naam" class="label">Naam</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-android-person"></span>
                                <input type="text" class="form-control" name="naam" value="{{old('naam')}}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email" class="label">Email</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-email"></span>
                                <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                <small> Dit wordt niet gedeeld, alleen gebruikt om berichten naar jou te sturen.</small>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date" class="label">Postcode</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-postalcode"></span>
                                <input type="text" class="form-control" id="postcode" name="postcode" value="{{old('postcode')}}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bedrijf" class="label">Bedrijf</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-company"></span>
                                <input type="bedrijf" class="form-control" id="bedrijf" name="bedrijf" value="{{old('bedrijf')}}">
                                <small> Dit veld is niet verplicht</small>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="wat_doe_je" class="label">Biedt je hulp aan of zoek je het?</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="wat_doe_je" id="wat_doe_je" class="form-control wat_doe_je">
                                    <option @if(old('wat_doe_je') == "volunteer") selected @endif value="volunteer">&nbsp;Ik bied mijn hulp aan</option>
                                    <option @if(old('wat_doe_je') == "seeker") selected @endif value="seeker">&nbsp;Ik heb hulp nodig</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="radius" class="label hide_seeker">Vanuit welke straal?</label>
                            <div class="form-field-icon-wrap hide_seeker">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="radius" id="radius" class="form-control">
                                    <option @if(old('radius') == 1) selected @endif value="1">&nbsp;< 5 km</option>
                                    <option @if(old('radius') == 2) selected @endif value="2">&nbsp;5-10km</option>
                                    <option @if(old('radius') == 4) selected @endif value="4">&nbsp;10-20km</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="categorie" class="label">Wat zoek je/ heb je nodig?</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="categorie" id="categorie" class="form-control">
                                    @foreach ($categories AS $category)
                                        <option @if(old('categorie') == $category->id) selected @endif value="{{$category->id}}">&nbsp {{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="vervoer" class="label hide_seeker">Hoe kan je het doen?</label>
                            <div class="form-field-icon-wrap hide_seeker">
                                <span class="icon ion-android-arrow-dropdown"></span>
                                <select name="vervoer" id="vervoer" class="form-control">
                                    @foreach ($transports AS $transport)
                                        <option @if(old('vervoer') == $transport->id) selected @endif value="{{$transport->id}}">&nbsp {{$transport->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="onderwerp" class="label">Onderwerp</label>
                            <div class="form-field-icon-wrap">
                                <span class="icon ion-onderwerp"></span>
                                <input type="text" class="form-control" id="onderwerp" name="onderwerp" value="{{old('onderwerp')}}" placeholder="Schrijf een kort onderwerp">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="beschrijving" class="label">Beschrijving</label>
                            <div class="form-field-icon-wrap">
                                <textarea class="form-control" name="beschrijving" placeholder="Beschrijf kort wat je aanbiedt of zoekt">@if (!empty(old('omschrijving'))) {{old('omschrijving') }}  @endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <input type="submit" class="btn btn-primary btn-outline-primary btn-block" value="Meld je nu aan">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!-- .section -->

<div class="section bg-light" id="section-overzicht" data-aos="fade-up">
    <div class="container">
        <div class="row section-heading justify-content-center mb-5">
            <div class="col-md-8 text-center">
                <h2 class="heading mb-3">Aangeboden en gezocht</h2>
                <p class="sub-heading mb-5">
                    Zie hier een klein overzicht van recente mensen/ bedrijven die hun diensten aanbieden
                    of hulp zoeken.
                </p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <ul class="nav site-tab-nav" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link change-supply-search" data-showonly="seeker" data-toggle="pill" href="#" role="tab" aria-controls="pills-breakfast" aria-selected="true">
                            Hulp gezocht
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link change-supply-search" data-showonly="volunteer" data-toggle="pill" href="#" role="tab" aria-controls="pills-lunch" aria-selected="false">
                            Hulp aangeboden
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active change-supply-search" data-showonly="all" data-toggle="pill" href="#" role="tab" aria-controls="pills-lunch" aria-selected="false">
                            Toon alles
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="pills-breakfast" role="tabpanel" aria-labelledby="pills-breakfast-tab">
                    @foreach ($supplies AS $supply)
                        <div class="d-block d-md-flex menu-food-item supply-item @if ($supply->is_volunteer) volunteer @else seeker @endif">
                            <div class="text order-2 mb-3">
                                @if ($supply->is_volunteer)
                                    <img src="images/helping.png" alt="Image">
                                @else
                                    <img src="images/help_wanted.png" alt="Image">
                                @endif
                                <h3>
                                    @if (!empty($supply->category->icon))
                                        <i class="fal fa-{{$supply->category->icon}}"></i>
                                    @else
                                        <i class="fal fa-hands-helping"></i>
                                    @endif
                                    <a href="#">{{$supply->people->name}}</a>
                                        - <small>{{$supply->category->name}}
                                        <br />
                                        {{$supply->topic}}</small>
                                </h3>
                                <p>{{$supply->description}}</p>
                                <a href="#" class="send-message" data-supply_id="{{$supply->id}}">
                                    <h4>
                                        <i class="fal fa-envelope"></i> Stuur een bericht
                                    </h4>
                                </a>
                            </div>
                            <div class="price order-3 text">
                                <strong>{{$supply->postal_code}}</strong>
                            </div>
                        </div> <!-- .menu-food-item -->
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- .section -->

<script>
    @if (count($supplies) > 0)
        var addMarkers = [
            @foreach ($supplies AS $supply)
                @if ($supply->long > 0 && $supply->lat > 0)
            {
                'long': {{$supply->long}},
                'lat': {{$supply->lat}},
                'title': '{{$supply->topic}}',
                'content': '<h4>{{$supply->people->name}}</h4>' +
                    '<h5>{{$supply->category->name}} - <small>{{$supply->topic}}</small></h5>' +
                    '<br />{{$supply->description}}<br /><br />' +
                    '<a href="#" class="send-message" data-supply_id="{{$supply->id}}"><h6><i class="fal fa-envelope"></i> Stuur een bericht</h6></a>' +
                    '',
                @if ($supply->is_volunteer)
                    icon: "https://maps.google.com/mapfiles/ms/icons/blue-dot.png",
                @else
                    icon: "https://maps.google.com/mapfiles/ms/icons/orange-dot.png",
                @endif
            },
                @endif
            @endforeach
        ];
    @else
        var addMarkers = [];
    @endif
</script>
<div class="section bg-light" id="section-overzicht" data-aos="fade-up" style="padding-top: 0;">
    <div class="container">
        <h2>Overzichts kaart </h2>
        <p>Op de onderstaande kaart vind je waar er hulp nodig is (oranje pins) en waar hulp geboden wordt (blauwe pins)</p>
    </div>
</div>

<div class="section bg-light" data-aos="fade-up">
   @include('maps')
</div>

@endsection
