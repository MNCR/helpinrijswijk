<?php $__env->startSection('content'); ?>
            <div class="cover_1 overlay bg-light">
                <div class="img_bg" style="background-image: url(images/teamwork.jpg);" data-stellar-background-ratio="0.5">
                    <div class="container">
                        <div class="row align-items-center justify-content-center text-center">
                            <div class="col-md-10" data-aos="fade-up">
                                <h2 class="heading mb-5">Help in Rijswijk</h2>
                                <p>
                                    <a href="#section-signin" class="smoothscroll btn btn-outline-white px-5 py-3">
                                        Schrijf je in!
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- .cover_1 -->













































            <div class="section pb-3 bg-white" id="section-hetidee" data-aos="fade-up">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-12 col-lg-8 section-heading">
                            <h2 class="heading mb-5">Het idee</h2>
                            <p>
                                De hele dag is het in het nieuws, het Corona virus. Thuis werken, thuis blijven en sociale afstand houden. Iedereen is er mee
                                bezig. Veel mensen hebben hulp nodig, en om die hulp in kaart te brengen hebben we Hulp in Rijswijk bedacht.
                            </p>
                            <p>
                                Vrijwilligers en bedrijven kunnen hier opgeven wat ze kunnen en willen doen (boodschappen doen, (gratis) maaltijden bezorgen,
                                dier verzorging of waar dan ook hulp bieden aan mensen die het nodig hebben.
                            </p>
                            <p>
                                Mensen die hulp zoeken kunnen een oproep plaatsen, maar ook in hun buurt zoeken naar mogelijkheden.
                            </p>
                        </div>
                    </div>
                </div>
            </div> <!-- .section -->













































































































































            <div class="section bg-light" data-aos="fade-up" id="section-signin">
                <div class="container">
                    <div class="row section-heading justify-content-center mb-5">
                        <div class="col-md-8 text-center">
                            <h2 class="heading mb-3">Meld je aan</h2>
                            <p class="sub-heading mb-5">Hulp nodig, of biedt je hulp, geef je op!</p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10 p-5 form-wrap">
                            <form action="#">
                                <div class="row mb-4">
                                    <div class="form-group col-md-4">
                                        <label for="name" class="label">Naam</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-android-person"></span>
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="email" class="label">Email</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-email"></span>
                                            <input type="email" class="form-control" id="email">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="email" class="label">Bedrijf</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-company"></span>
                                            <input type="company" class="form-control" id="company">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="date" class="label">Postcode</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-postalcode"></span>
                                            <input type="text" class="form-control" id="postalcode">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="persons" class="label">Wat wil/doe je?</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-android-arrow-dropdown"></span>
                                            <select name="persons" id="persons" class="form-control">
                                                <option value="1">&nbsp;Ik bied mijn hulp aan</option>
                                                <option value="2">&nbsp;Ik heb hulp nodig</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="persons" class="label">Vanuit welke straal?</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-android-arrow-dropdown"></span>
                                            <select name="persons" id="persons" class="form-control">
                                                <option value="1">&nbsp;< 5 km</option>
                                                <option value="2">&nbsp;5-10km</option>
                                                <option value="3">&nbsp;10-20km</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="persons" class="label">Wat wil/ kan je doen?</label>
                                        <div class="form-field-icon-wrap">
                                            <span class="icon ion-android-arrow-dropdown"></span>
                                            <select name="category" id="category" class="form-control">
                                                <option value="1">&nbsp;Boodschappen</option>
                                                <option value="2">&nbsp;Dieren verzorging</option>
                                                <option value="3">&nbsp;(Online) luisterend oor</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label for="persons" class="label">Beschrijving</label>
                                        <div class="form-field-icon-wrap">
                                            <textarea class="form-control">Beschrijf hier in het kort wat je biedt.</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <input type="submit" class="btn btn-primary btn-outline-primary btn-block" value="Schrijf je nu in">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- .section -->

            <div class="section bg-light" id="section-menu" data-aos="fade-up">
                <div class="container">
                    <div class="row section-heading justify-content-center mb-5">
                        <div class="col-md-8 text-center">
                            <h2 class="heading mb-3">Aangeboden en gezocht</h2>
                            <p class="sub-heading mb-5">
                                Zie hier een klein overzicht van recente mensen/ bedrijven die hun diensten aanbieden
                                of hulp zoeken.
                            </p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <ul class="nav site-tab-nav" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-breakfast-tab" data-toggle="pill" href="#pills-breakfast" role="tab" aria-controls="pills-breakfast" aria-selected="true">
                                        Hulp aangeboden
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-lunch-tab" data-toggle="pill" href="#pills-lunch" role="tab" aria-controls="pills-lunch" aria-selected="false">
                                        Hulp gezocht
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-breakfast" role="tabpanel" aria-labelledby="pills-breakfast-tab">
                                    <div class="d-block d-md-flex menu-food-item">

                                        <div class="text order-1 mb-3">
                                            <img src="images/img_1.jpg" alt="Image">
                                            <h3><a href="#">Warm Spinach Dip &amp; Chips</a></h3>
                                            <p>Spinach and artichokes in a creamy cheese dip with warm tortilla chips &amp; salsa.</p>
                                        </div>
                                        <div class="price order-2">
                                            <strong>$10.49</strong>
                                        </div>
                                    </div> <!-- .menu-food-item -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- .section -->



























































<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/michiel/sites/helpinrijswijk/resources/views/welcome.blade.php ENDPATH**/ ?>
