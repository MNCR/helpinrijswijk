<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Supply extends Model
{

    use SoftDeletes;

    /**
     * The attributate that are fillable by users
     *
     * @var array
     */
    protected $fillable = [
        'people_id',
        'transport_id',
        'category_id',
        'postal_code',
        'radius',
        'supply_verified',
        'verify_token',
        'is_volunteer',
        'client_ip',
        'long',
        'lat',
        'topic',
        'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function people() {
        return $this->belongsTo('App\People');
    }

    public function transport() {
        return $this->belongsTo('App\Transport');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }
}
