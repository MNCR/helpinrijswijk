<?php

namespace App\Mail;

use App\Supply;
use App\People;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $supply, $people;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Supply $supply, People $people)
    {
        $this->supply = $supply;
        $this->people = $people;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('helpinrijswijk@gmail.com')
            ->subject('Er is een nieuw bericht voor je')
            ->view('emails.new_message');
    }
}
