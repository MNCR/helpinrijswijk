<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\People;

class MessageCheckEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $people, $route;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(People $people, $route)
    {
        $this->people = $people;
        $this->route = $route;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('helpinrijswijk@gmail.com')
            ->subject('Link om informatie te bekijken!')
            ->view('emails.check_email');
    }
}
