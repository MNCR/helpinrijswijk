<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Supply;

class SupplyAdded extends Mailable
{
    use Queueable, SerializesModels;

    public $supply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Supply $supply)
    {
        $this->supply = $supply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('helpinrijswijk@gmail.com')
            ->subject('Dank voor je bijdrage!')
            ->view('emails.supply_verify');
    }
}
