<?php

namespace App\Mail;

use App\Supply;
use App\People;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;


    public $people;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(People $people)
    {
        $this->people = $people;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('helpinrijswijk@gmail.com')
            ->subject('Verifier je e-mail adres')
            ->view('emails.email_verify');
    }
}
