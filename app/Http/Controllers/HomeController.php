<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

//Mailers
use App\Mail\MessageCheckEmail;
use App\Mail\NewMessage;
use App\Mail\SupplyAdded;

//Models
use App\Category;
use App\Transport;
use App\Supply;
use App\People;
use App\Message;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Left empty
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $transports = Transport::all();
        $categories = Category::all();
        $supplies = Supply::where('supply_verified', true)->orderBy('created_at', 'desc')->get();
        $homepage = true; //We need this to hack about the header ;-)

        return view('start', compact('transports', 'categories', 'supplies', 'homepage'));
    }

    public function privacy() {
        return view('privacy');
    }

    public function verifyEmail($code) {
        $people = People::where('verify_token', '=', $code)->first();
        $people->email_verified = true;
        $people->save();

        foreach ($people->supplies AS $supply) {
            $supply->verify_token = $code;
            $supply->supply_verified = true;
            $supply->save();
        }

        $messages = Message::where('from_people_id', '=', $people->id)->where('is_verified', '=', false)->get();
        foreach ($messages AS $message) {
            $message->message_token = $code;
            $message->is_verified = true;
            $message->save();

            Mail::to($message->toPeople->email)->send(new NewMessage($message->supply, $message->toPeople));
        }

        return redirect(route('home'))->with('email_verified', 'email_verified');
    }

    public function howDoesItWork() {
        return view('howdoesitwork');
    }
}
