<?php

namespace App\Http\Controllers;

use App\People;
use App\Supply;
use App\Message;

use App\Mail\MessageCheckEmail;
use App\Mail\NewMessage;
use App\Mail\VerifyEmail;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function messages() {
        return view('messages.index');
    }

    public function sendEmail(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails())
            return ['result' => false, 'message' => 'That is not a correct e-mail address'];

        $people = People::where('email', '=', $request->email)->first();
        if (!empty($people)) {
            Mail::to($people->email)->send(new MessageCheckEmail($people, 'messages.my_messages'));
        }

        return ['result' => true, 'message' => 'Indien dat e-mail adres bestaat, hebben wij je een e-mail met een link gestuurd'];
    }

    public function myMessages($code) {
        $people = People::where('verify_token', '=', $code)->first();

        if (empty($people)) {
            return redirect(route('home'))->with([
                'error_message' => 'De opgegeven code is niet gekoppeld aan een account, of is verlopen. Probeer het opnieuw'
            ]);
        }

        $messages = Message::where('message_id', NULL)
            ->where(function ($query) use ($people) {
                $query->where('to_people_id', $people->id)
                    ->orWhere('from_people_id', $people->id);
            })->get();

        return view('messages.my_messages', compact('people', 'code', 'messages'));
    }

    public function newMessage(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
            'supply_id' => 'required|integer|exists:supplies,id',
        ]);

        if ($validator->fails())
            return ['result' => false, 'message' => 'Dat ging niet goed'];

        $data = $validator->valid();
        $supply = Supply::find($data['supply_id']);
        $people = People::where('email', '=', $data['email'])->first();
        $uniqueID = uniqid (rand (),true).uniqid (rand (),true);

        if (empty($people)) {
            $people = People::create(
                [
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'is_volunteer' => ($supply->is_volunteer) ? false : true,
                    'verify_token' => $uniqueID,
                ]
            );
        }

        Message::create([
            'from_people_id' => $people->id,
            'to_people_id' => $supply->people->id,
            'supply_id' => $supply->id,
            'message' => $data['message'],
            'message_token' => $uniqueID,
        ]);

        if ($people->email_verified) {
            Mail::to($supply->people->email)->send(new NewMessage($supply, $supply->people));
        } else {
            Mail::to($people->email)->send(new VerifyEmail($people));
        }

        return ['result' => true, 'message' => 'Bericht is verstuurd, als je nog niet eerder berichten gepost had, krijg je eerst een verificatie e-mail.'];
    }

    public function replyMessage(Message $message, $code) {
        //Check to see if the code matches the users verification code
        if ($message->toPeople->verify_token !== $code && $message->fromPeople->verify_token !== $code)
            return redirect('home');

        return view('messages.reply', compact('message', 'code'));
    }

    public function sendReplyMessage(Request $request, Message $message, $code) {
        $data = $request->validate(['bericht' => 'required|string|min:10|max:200']);
        //Check to see if the code matches the users verification code
        if ($message->toPeople->verify_token !== $code && $message->fromPeople->verify_token !== $code)
            return redirect('home');

        if ($message->toPeople->verify_token === $code) {
            $from = $message->toPeople;
            $to = $message->fromPeople;
        } else {
            $to = $message->toPeople;
            $from = $message->fromPeople;
        }

        Message::create([
            'message_id' => $message->id,
            'supply_id' => $message->supply_id,
            'to_people_id' => $to->id,
            'from_people_id' => $from->id,
            'message' => $data['bericht'],
            'is_verified' => true,
            'message_token' => $code,
        ]);

        Mail::to($to->email)->send(new NewMessage($message->supply, $to));

        return redirect(route('messages.my_messages', [$code]))
            ->with([
                'success_message' => 'Je bericht is verstuurd.'
            ]);;
    }
}

