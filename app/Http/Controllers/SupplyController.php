<?php

namespace App\Http\Controllers;

use App\Mail\MessageCheckEmail;

use App\Transport;
use App\Category;
use App\Supply;
use App\People;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\Dutchpostalcode;
use Illuminate\Support\Facades\Mail;
use App\Mail\SupplyAdded;
use Spatie\Geocoder\Geocoder;

class SupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplies.index');
    }

    public function sendEmail(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails())
            return ['result' => false, 'message' => 'That is not a correct e-mail address'];

        $people = People::where('email', '=', $request->email)->first();
        if (!empty($people)) {
            Mail::to($people->email)->send(new MessageCheckEmail($people, 'supplies.my_supplies'));
        }

        return ['result' => true, 'message' => 'Indien dat e-mail adres bestaat, hebben wij je een e-mail met een link gestuurd'];
    }

    public function mySupplies($code) {
        $people = People::where('verify_token', '=', $code)->first();

        if (empty($people)) {
            return redirect(route('home'))->with([
                'error_message' => 'De opgegeven code is niet gekoppeld aan een account, of is verlopen. Probeer het opnieuw'
            ]);
        }

        $supplies = Supply::where('people_id', $people->id)->get();
        return view('supplies.my_supplies', compact('people', 'code', 'supplies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'naam' => 'required|string',
            'email' => 'required|email',
            'bedrijf' => 'nullable|string',
            'postcode' => ['required', 'string', new Dutchpostalcode],
            'wat_doe_je' => 'required|in:volunteer,seeker',
            'radius' => 'required|integer',
            'categorie' => 'required|integer|exists:categories,id',
            'vervoer' => 'required|integer|exists:transports,id',
            'onderwerp' => 'required|string|max:200',
            'beschrijving' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('/#section-meldjeaan')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $validator->valid();

        $uniqueID = uniqid (rand (),true).uniqid (rand (),true);

        //Change postal code to correct postalcode
        $data['postcode'] = str_replace(' ', '', strtoupper($data['postcode']));
        //Get the possible user (still need to verify their job)
        $user = People::where('email', '=', $data['email'])->first();

        //Is this a voluntary thing, or not. Might be user, might be on supply
        $volunteer = true;
        if ($data['wat_doe_je'] == 'seeker')
            $volunteer = false;

        //If the user does not exist, create the user
        if (empty($user)) {
            $company = null;

            if (!empty($data['company']))
                $company = $data['company'];

            $user = People::create(
                [
                    'name' => $data['naam'],
                    'email' => $data['email'],
                    'company' => $company,
                    'is_volunteer' => $volunteer,
                    'verify_token' => $uniqueID,
                ]
            );
        }

        switch ($data['radius']) {
            case 1:
                $radius = 5;
                break;
            case 2:
                $radius = 10;
                break;
            case 4:
                $radius = 20;
                break;

            default:
                $radius = 5;
        }

        $client = new \GuzzleHttp\Client();
        $geocoder = new Geocoder($client);
        $geocoder->setApiKey(config('geocoder.key'));
        $geocoder->setCountry(config('geocoder.country', 'NL'));
        $geoData = $geocoder->getCoordinatesForAddress($data['postcode']);
        $result = $geoData['accuracy'];

        //Add the supply
        $supply = Supply::create(
            [
                'people_id' => $user->id,
                'transport_id' => $data['vervoer'],
                'category_id' => $data['categorie'],
                'postal_code' => $data['postcode'],
                'radius' => $radius,
                'is_volunteer' => $volunteer,
                'topic' => $data['onderwerp'],
                'description' => $data['beschrijving'],
                'verify_token' => $uniqueID,
                'long' => $geoData['lng'],
                'lat' => $geoData['lat'],
                'client_ip' => $_SERVER['REMOTE_ADDR'],
            ]
        );

        Mail::to($user->email)->send(new SupplyAdded($supply));

        return redirect(route('home'))->with('supply_added', 'supply_added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function show(Supply $supply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function edit(Supply $supply, $code)
    {
        if ($supply->people->verify_token !== $code) {
            return redirect(route('home'))->with([
                'error_message' => 'De opgegeven code is niet correct, verlopen of hoort niet bij de opgegeven oproep.'
            ]);
        }

        $categories = Category::all();
        $transports = Transport::all();

        return view('supplies.edit', compact('supply', 'code', 'transports', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supply $supply, $code)
    {
        if ($supply->people->verify_token !== $code) {
            return redirect(route('home'))->with([
                'error_message' => 'De opgegeven code is niet correct, verlopen of hoort niet bij de opgegeven oproep.'
            ]);
        }

        $data = $request->validate([
            'radius' => 'required|integer',
            'categorie' => 'required|integer|exists:categories,id',
            'vervoer' => 'required|integer|exists:transports,id',
            'onderwerp' => 'required|string|max:200',
            'beschrijving' => 'required|string',
        ]);

        $supply->radius = $data['radius'];
        $supply->category_id = $data['categorie'];
        $supply->transport_id = $data['vervoer'];
        $supply->topic = $data['onderwerp'];
        $supply->description = $data['beschrijving'];
        $supply->save();

        return redirect(route('supplies.my_supplies', [$code]))->with([
            'success_message' => 'Jouw oproep is bijgewerkt.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supply $supply, $code)
    {
        if ($supply->people->verify_token !== $code) {
            return redirect(route('home'))->with([
                'error_message' => 'De opgegeven code is niet correct, verlopen of hoort niet bij de opgegeven oproep.'
            ]);
        }

        $supply->delete();
        return redirect(route('supplies.my_supplies', [$code]))->with([
            'success_message' => 'De oproep is gewist.'
        ]);
    }
}
