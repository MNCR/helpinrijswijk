<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
     * The attributate that are fillable by users
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'email_verified',
        'company',
        'is_volunteer',
        'verify_token',
    ];

    public function supplies() {
        return $this->hasMany('App\Supply');
    }

    public function mySendMessages() {
        return $this->hasMany('App\Message', 'from_people_id', 'id')
            ->where('message_id',null)
            ->orderBy('is_read')
            ->orderBy('created_at');
    }

    public function myReceivedMessages() {
        return $this->hasMany('App\Message', 'to_people_id', 'id')
            ->where('message_id',null)
            ->orderBy('is_read')
            ->orderBy('created_at');
    }
}
