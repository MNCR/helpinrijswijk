<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributate that are fillable by users
     *
     * @var array
     */
    protected $fillable = [
        'from_people_id',
        'to_people_id',
        'supply_id',
        'topic',
        'message',
        'is_read',
        'is_verified',
        'message_token',
        'message_id',
    ];

    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];

    public function messages() {
        return $this->hasMany('App\Message');
    }

    public function supply() {
        return $this->belongsTo('App\Supply');
    }

    public function toPeople() {
        return $this->belongsTo('App\People', 'to_people_id', 'id');
    }
    public function fromPeople() {
        return $this->belongsTo('App\People', 'from_people_id', 'id');
    }
}
